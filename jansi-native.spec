%global bits %{__isa_bits}
%global debug_package %{nil}

Name:           jansi-native
Version:        1.7
Release:        8
Summary:        Jansi Native implements the JNI Libraries used by the Jansi project
License:        ASL 2.0
URL:            http://jansi.fusesource.org/
Source0:        https://github.com/fusesource/jansi-native/archive/jansi-native-%{version}.tar.gz

BuildRequires:  maven-local mvn(junit:junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.fusesource:fusesource-pom:pom:)
BuildRequires:  mvn(org.fusesource.hawtjni:hawtjni-runtime) >= 1.9-2
BuildRequires:  mvn(org.fusesource.hawtjni:maven-hawtjni-plugin) >= 1.9-2

Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description
Jansi is a small ASl 2.0 licensed Java library that allows you to use ANSI
escape sequences to format your console output which works even on windows.

%prep
%setup -q -n jansi-native-jansi-native-%{version}
%mvn_package :::linux%{bits}:
%mvn_alias :jansi-linux%{bits} :jansi-linux
%mvn_file :jansi-linux%{bits} %{name}/jansi-linux%{bits} %{name}/jansi-linux
sed -i '36c CFLAGS="$CFLAGS -fstack-protector -Wl,-s"' src/main/native-package/m4/custom.m4

%build
%mvn_build
%mvn_build -- -Dplatform=linux%{bits}

%install
%mvn_install

%files
%defattr(-,root,root)
%doc readme.md changelog.md
%license license.txt
%{_prefix}/lib/java/*
%{_datadir}/javadoc/*
%{_datadir}/maven-poms/*
%{_datadir}/maven-metadata/*

%changelog
* Mon Mar 6 2023 caodongxia <caodongxia@h-partners.com> - 1.7-8
- Fix not stripped problem

* Mon Sep 14 2020 maminjie <maminjie1@huawei.com> - 1.7-7
- Add jansi-linux jar packages

* Sat Dec 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.7-6
- Package init
